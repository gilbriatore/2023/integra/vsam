﻿class GravadorVsam{

  static void Main(){

    var cts = new CancellationTokenSource();
    Task.Run(async () => {
      while (!cts.Token.IsCancellationRequested){
        Gravar();  
        await Task.Delay(TimeSpan.FromSeconds(5));
        Apagar();  
        await Task.Delay(TimeSpan.FromSeconds(5));
      } 
    }, cts.Token);

    Console.ReadKey();
  }

  static void Apagar(){
    File.Delete("C:\\_src\\_integra\\VSAM\\VSAM0080.CWRT");  
    Console.WriteLine("Excluído!");
  }

  static void Gravar(){
    string[] linhas = {
      "000100 Anderson       Adrian    111 Peachtree Plaza     Atlanta        GA 26101 ", 
      "001100 Kemper         Kelly     1111 Oak Circle         Kansas City    KS 55651", 
      "002600 Zenith         Zebulon   2626 26TH Street        Dallas         TX 71922"};

    using var arquivo = new StreamWriter("C:\\_src\\_integra\\VSAM\\VSAM0080.CWRT");  
    foreach (var linha in linhas)
    {
      arquivo.WriteLine(linha);
    }
    arquivo.Close();
    Console.WriteLine("Gravado!");
  }
}