package br.edu.up.vsam;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.io.File;

public class GravadorVsam {

	public static void main(String[] args) throws IOException {

		new Timer().schedule(new TimerTask() {
		    @Override
		    public void run() {
		    	
		        try {
		        	gravar();
		            TimeUnit.SECONDS.sleep(5);
		            excluir();
		            TimeUnit.SECONDS.sleep(5);
		        } catch (InterruptedException e) {
		            throw new RuntimeException(e);
		        }
		    }
		}, 0, 1000);

	}
	
	public static void gravar() {
		String[] linhas = {
	    	      "000100 Anderson       Adrian    111 Peachtree Plaza     Atlanta        GA 26101 ", 
	    	      "001100 Kemper         Kelly     1111 Oak Circle         Kansas City    KS 55651", 
	    	      "002600 Zenith         Zebulon   2626 26TH Street        Dallas         TX 71922"};
		FileWriter arquivo;
		try {
			arquivo = new FileWriter("C:\\_src\\_integra\\VSAM\\VSAM0080.JWRT");
			PrintWriter gravador = new PrintWriter(arquivo);
			for (String linha : linhas) {
				gravador.println(linha);
				
			}
			gravador.close();
			System.out.println("Gravado!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void excluir() {
		File arquivo = new File("C:\\_src\\_integra\\VSAM\\VSAM0080.JWRT");
		arquivo.delete();
		System.out.println("Excluído!");
	}
}





