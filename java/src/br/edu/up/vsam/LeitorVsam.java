package br.edu.up.vsam;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class LeitorVsam {

	public static void main(String[] args) throws FileNotFoundException {
		
		File arquivo = new File("C:\\_src\\_integra\\VSAM\\VSAM0080.DAT");
		Scanner leitor = new Scanner(arquivo);
		
		while(leitor.hasNext()) {
			String linha = leitor.nextLine();
			String codigo = linha.substring(0, 6);
			String nome = linha.substring(7, 22);
			String cidade = linha.substring(56, 71);
			
			System.out.println("Código: " + codigo + " Nome: " + nome + " Cidade: " + cidade);
		}
		
		leitor.close();

	}

}
